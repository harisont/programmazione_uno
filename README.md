This is a programming university project I wrote together with my dear friends Gioara and Nifty during our first year.
It is now online as I am giving programming lesson to another student and is often useful to compare my implementation as a beginner with his own.

How to compile:

```
$ gcc -c progexam.c
$ gcc -c mylib.c
$ gcc -o progexam progexam.o mylib.o
```

Further information about the project is available in Italian in the two following files:

- [leggimi.txt](https://gitlab.com/harisont/programmazione_uno/blob/master/leggimi.txt)
- [consegna.pdf](https://gitlab.com/harisont/programmazione_uno/blob/master/consegna.pdf)
