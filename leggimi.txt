Questo progetto è stato realizzato da:
- Arianna Masciolini
- Gioara Roberta Cuccu
- Clarissa Poleri Alunno


Di seguito sono elencate e brevemente descritte le differenze rispetto a quanto richiesto dal testo d'esame, opportunamente motivate:

* All'avvio, verrà richiesto di inserire le proprie iniziali. Per il momento, ciò rimane senza uno scopo preciso ma potrebbe essere utile, in prospettiva, per identificare l'utente che ha, ad esempio, preso in prestito un determinato libro.

* L'ID, interpretato come numero d'inventario, è stato reso sequenziale, assegnato in automatico ad ogni nuovo elemento, in modo da fornire informazioni sull'ordine di inserimento, rendendo così significativo l'ordinamento per id, e da garantire univocità senza bisogno di ulteriori controlli. 

* Su richiesta dell'utente alla chiusura del programma, i dati possono essere salvati su un file binario; questo per rendere il programma potenzialmente utilizzabile e, allo stesso tempo, per velocizzarne il collaudo. Conseguentemente, sono state aggiunte due funzioni  -una per il salvataggio dei dati e una per il caricamento- più una terza per il reset automatico dell'intera lista.

* Per realizzare la funzione di ordinamento lessicografico abbiamo creato una funzione aggiuntiva che compara due stringhe equiparando maiuscole e minuscole ('A' ed 'a', ad esempio, vengono interpretate con lo stesso valore, cosa che non accadrebbe con strcmp()). I numeri hanno priorità sulle lettere.

* Nel menù è presente solo un' opzione di inserimento, perchè solo in seguito, e solo a partire dal secondo inserimento, avrà un significato decidere della sua collocazionein testa o in coda.

* Alla fine di ogni inserimento è presente la verifica dei dati inseriti con il richiamo di una funzione che stampa il singolo elemento, riutilizzata anche in tutte le altre funzioni di stampa. In caso di errore, si può decidere di sovrascrivere il record.

* Le funzioni nel menù sono state organizzate in base all'affinità dei loro compiti, rendendo suddetto menù il più possibile sintetico.

* La funzione rmvCharAuthor() è stata trasformata in una funzione chiamata scherzosamente "Abiura", che elimina tutti i libri di un dato Autore. Nella sua realizzazione è stata usata la funzione strcmp() della libreria standard string.h, pertanto l'abiura è case-sensitive. 

* Per un caso fortuito le iniziali dei nostri soprannomi sono G, N ed U. E' sembrato naturale dunque sfruttare questa coincidenza nel dare un nome al programma come si può notare dalla schermata di accesso.
